# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/tushar/ORB_SLAM2/Examples/ROS/ORB_SLAM2/src/ros_stereo.cc" "/home/tushar/ORB_SLAM2/Examples/ROS/ORB_SLAM2/build/CMakeFiles/Stereo.dir/src/ros_stereo.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "COMPILEDWITHC11"
  "ROS_PACKAGE_NAME=\"ORB_SLAM2\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "/home/tushar/indigo_ws/devel/include"
  "/home/tushar/indigo_ws/src/geometry_experimental/tf2_ros/include"
  "/home/tushar/indigo_ws/src/geometry_experimental/tf2/include"
  "/home/tushar/indigo_ws/src/geometry_experimental/tf2_msgs/include"
  "/home/tushar/indigo_ws/src/image_common/image_transport/include"
  "/opt/ros/indigo/include"
  "/usr/include/opencv"
  "/usr/local/include/opencv"
  "/usr/local/include"
  ".."
  "../../../.."
  "../../../../include"
  "/usr/local/lib/cmake/Pangolin/../../../include"
  "/usr/include/eigen3"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
